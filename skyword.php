<?php 
/*
Plugin Name: Skyword
Plugin URI: http://www.skyword.com
Description: Integration with the Skyword content publication platform.
Version: 2.2
Author: Skyword, Inc.
Author URI: http://www.skyword.com
License: GPL2
*/

/*  Copyright 2014  Skyword, Inc.     This program is free software; you can redistribute it and/or modify    it under the terms of the GNU General Public License, version 2, as    published by the Free Software Foundation.     This program is distributed in the hope that it will be useful,    but WITHOUT ANY WARRANTY; without even the implied warranty of    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    GNU General Public License for more details.     You should have received a copy of the GNU General Public License    along with this program; if not, write to the Free Software    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA */ 

if ( !defined('SKYWORD_PATH') )
	define( 'SKYWORD_PATH', plugin_dir_path( __FILE__ ) );
if ( !defined('SKYWORD_VERSION') )
	define( 'SKYWORD_VERSION', "2.2" );
if ( !defined('SKYWORD_VN') )
	define( 'SKYWORD_VN', "2.2" ); //This CANNOT have two decimal places. 2
//.1.4 is NOT valid.

// Default class names for developer options
// Default class names for inline media
if ( !defined('SKYWORD_INLINE_LEFT_DEFAULT') )
	define( 'SKYWORD_INLINE_LEFT_DEFAULT', 'sw-img__float-left' );
if ( !defined('SKYWORD_INLINE_RIGHT_DEFAULT') )
	define( 'SKYWORD_INLINE_RIGHT_DEFAULT', 'sw-img__float-right' );
if ( !defined('SKYWORD_INLINE_CENTER_DEFAULT') )
	define( 'SKYWORD_INLINE_CENTER_DEFAULT', 'sw-img__center' );

// Default class names for shortcodes
if ( !defined('SKYWORD_CAPTION_PARENT_DEFAULT') )
	define( 'SKYWORD_CAPTION_PARENT_DEFAULT', 'sw-caption' );
if ( !defined('SKYWORD_CAPTION_TEXT_DEFAULT') )
	define( 'SKYWORD_CAPTION_TEXT_DEFAULT', 'sw-caption__text' );

register_activation_hook(__FILE__, 'get_skyword_defaults');

// Set defaults on initial plugin activation
function get_skyword_defaults() {
	$tmp = get_option('skyword_plugin_options');
    if(!is_array($tmp)) {
		$arr = array(
		"skyword_api_key" => null, 
		"skyword_enable_ogtags" => true, 
		"skyword_enable_metatags" => true, 
		"skyword_enable_googlenewstag" => true,
		"skyword_enable_pagetitle" => true,
		"skyword_enable_sitemaps" => true,
		"skyword_generate_all_sitemaps" => true,
		"skyword_generate_news_sitemaps" => true,
		"skyword_generate_pages_sitemaps" => true,
		"skyword_generate_categories_sitemaps" => true,
		"skyword_generate_tags_sitemaps" => true,
    "skyword_dev_option_image_left" => null,
    "skyword_dev_option_image_right" => null,
    "skyword_dev_option_image_center" => null,
    "skyword_dev_option_caption_parent" => null,
    "skyword_dev_option_caption_text" => null
		);
		update_option('skyword_plugin_options', $arr);
	}
}

require SKYWORD_PATH.'php/class-skyword-publish.php';
require SKYWORD_PATH.'php/class-skyword-sitemaps.php';
require SKYWORD_PATH.'php/class-skyword-shortcode.php';
require SKYWORD_PATH.'php/class-skyword-opengraph.php';
require SKYWORD_PATH.'php/options.php';
