<?php 
class Skyword_Shortcode {

	function __construct() {
		@add_shortcode( 'cf', array($this, 'customfields_shortcode') );
		@add_shortcode( 'skyword_tracking', array($this, 'skyword_tracking') );
		@add_shortcode( 'sw_caption', array($this, 'sw_caption_shortcode') );
	}

	function customfields_shortcode( $atts, $text ) {
		global $post;
		return get_post_meta( $post->ID, $text, true );
	}

	function skyword_tracking($atts){
		global $post;

		if(!isset($atts['id'])){
			$atts['id'] = get_post_meta($post->ID, 'skyword_content_id', true);
		}

		return "<script async='' type='text/javascript' src='//tracking.skyword.com/tracker.js?contentId={$atts['id']}'></script>";
	}

	function sw_caption_shortcode( $atts, $content = null ) {
    $options = get_option('skyword_plugin_options');
    $parent_class = ($options['skyword_dev_option_caption_parent'] != '') ? $options['skyword_dev_option_caption_parent'] : SKYWORD_CAPTION_PARENT_DEFAULT; # get class used for caption parent <div>
    $text_class = ($options['skyword_dev_option_caption_text'] != '') ? $options['skyword_dev_option_caption_text'] : SKYWORD_CAPTION_TEXT_DEFAULT; # get class used for caption text <p>
    $caption = '<p class="'.$text_class.'">'.$atts['text'].'</p>';
    ob_start(); # allows me to output html below w/out echo'ing all markup to a variable
    ?>
    <?php if (!empty($content)) {
      # Find out if we'll need to add a left or right class to the caption parent <div> below
      # NOTE: This is assuming that $content has an image tag in it
      $img_left_class = ($options['skyword_dev_option_image_left'] != '') ? $options['skyword_dev_option_image_left'] : SKYWORD_INLINE_LEFT_DEFAULT; # get class used for float-left images
      $img_right_class = ($options['skyword_dev_option_image_right'] != '') ? $options['skyword_dev_option_image_right'] : SKYWORD_INLINE_RIGHT_DEFAULT; # get class used for float-right images
      $left_right = ""; # default to no left/right aligning class
      if (preg_match('/<img.*class=".*'.$img_left_class.'/', $content)){
        $left_right = $parent_class."--left";
      } else if (preg_match('/<img.*class=".*'.$img_right_class.'/', $content)){
        $left_right = $parent_class."--right";
      }
    ?>
      <div class="<?php echo $parent_class . ' ' . $left_right; ?>">
        <?php 
        # If position is set and equals top
        if (!empty($atts['position']) && strtolower($atts['position']) == 'top') {
          echo $caption; # output caption
        }
        # do_shortcode($content)
        # allows Skyword or user to include a shortcode in $content
        echo  do_shortcode($content); # output content
        # If position is not set OR equals bottom
        if (empty($atts['position']) || strtolower($atts['position']) == 'bottom') {
          echo $caption; # output caption
        }
        ?>
      </div>
    <?php
    } else {
      echo '<p class="'.$text_class.' '.$text_class.'--alone">'.$atts['text'].'</p>'; # output caption
    }
    return ob_get_clean(); # get's the above html output after ob_start()
	}
}
global $custom_shortcodes;
$custom_shortcodes = new Skyword_Shortcode;
